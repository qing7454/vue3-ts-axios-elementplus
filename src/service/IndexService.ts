import HttpClient from '@/http'
import { IndexData } from '@/model/IndexData';
import { ResponseData } from '@/model/ResponseData'
/**
 * @interface loginParams -登录参数
 * @property {string} username -用户名
 * @property {string} password -用户密码
 */
interface LoginParams {
  username: string
  password: string
}
//封装User类型的接口方法
export class IndexService {
  /**
   * @description 查询User的信息
   * @param {number} teamId - 所要查询的团队ID
   * @return {HttpResponse} result
   */
  static async login(params: LoginParams): Promise<ResponseData<any>> {
    return HttpClient('/api/user', {
      method: 'get',
      responseType: 'json',
      params: {
        ...params
      },
    })
  }

  static async resgister(params: LoginParams): Promise<ResponseData<any>> {
    return HttpClient('/api/user/resgister', {
      method: 'get',
      responseType: 'json',
      params: {
        ...params
      },
    })
  }

  static async getIndexData(): Promise<ResponseData<IndexData>> {
      return (await HttpClient('/index/index', {method: 'GET', responseType: 'text', params: {}})).data;
  }
}