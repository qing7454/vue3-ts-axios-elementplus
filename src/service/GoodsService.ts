import { ResponseData } from "@/model/ResponseData";
import HttpClient from "@/http"

interface SearchGoodsPrpams {
    name: string
}

export class GoodsService {
    static async getList(params: SearchGoodsPrpams): Promise<ResponseData<any>> {
        return HttpClient('/api/user', {
            method: 'get',
            responseType: 'json',
            params: {
              ...params
            }
        });
    }
}