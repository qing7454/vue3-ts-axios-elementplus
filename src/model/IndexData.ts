import { Banner } from "./Banner";
import { Brand } from "./Brand";
import { Category } from "./Category";
import { Channel } from "./Channel";
import { HotGoods } from "./HotGoods";
import { NewGoods } from "./NewGoods";
import { Topic } from "./Topic";

export interface IndexData {
    banner: Banner[],
    brandList: Brand[],
    categoryList: Category[],
    channel: Channel[],
    hotGoodsList: HotGoods[],
    newGoodsList: NewGoods[],
    topicList: Topic[]
}