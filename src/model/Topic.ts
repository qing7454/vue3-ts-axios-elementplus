export interface Topic {
    avatar: string,
    content: string,
    id: number,
    is_show: number,
    item_pic_url: string,
    price_info: number,
    read_count: string,
    scene_pic_url: string,
    sort_order: number,
    subtitle: string,
    title: string,
    topic_category_id: number,
    topic_tag_id: number,
    topic_template_id: number
}