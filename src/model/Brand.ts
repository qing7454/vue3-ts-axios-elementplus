export interface Brand {
    app_list_pic_url: string,
    floor_price: number,
    id: number,
    is_new: number,
    is_show: number,
    list_pic_url: string,
    name: string,
    new_pic_url: string,
    new_sort_order: number,
    pic_url: string,
    simple_desc: string,
    sort_order: number
}