export interface Channel {
    icon_url: string,
    id: number,
    name: string,
    sort_order: number,
    url: string
}