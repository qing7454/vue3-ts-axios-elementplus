import { CategoryGoods } from "./CategoryGoods";

export interface Category {
    id: number,
    name: string,
    goodsList: CategoryGoods[]
}