export interface Banner {
    ad_position_id: number,
    content: string,
    enabled: number,
    end_time: number,
    id: number,
    image_url: string,
    link: string,
    media_type: number,
    name: string
}