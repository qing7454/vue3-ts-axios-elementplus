export interface NewGoods {
    id: number,
    list_pic_url: string,
    name: string,
    retail_price: number
}