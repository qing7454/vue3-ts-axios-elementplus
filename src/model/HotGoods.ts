export interface HotGoods {
    goods_brief: string,
    id: number,
    list_pic_url: string,
    name: string
    retail_price: number
}